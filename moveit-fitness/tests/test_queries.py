from moveit_fitness.queries import *
from moveit_fitness.tables import create_table_if_not_exists, destroy_table
from moveit_fitness.seed_data import seed_table
from tests.values import *
from pprint import pprint as pp

def test_get_sample_item():
    """
    GIVEN table stored in the database
    WHEN get_sample_item is called
    THEN it should return a dictionary representation of a single item from that table
    """
    create_table_if_not_exists(TEST_TABLE_NAME)
    seed_table(TEST_TABLE_NAME, 3)
    item = get_sample_item(TEST_TABLE_NAME)
    assert isinstance(item, dict)
    assert len(item.keys()) > 0
    #destroy_table('test')


def test_list_items():
    """
    GIVEN articles stored in the database
    WHEN endpoint /article-list/ is called
    THEN it should return list of Article in json format that matches the schema
    """
    create_table_if_not_exists(TEST_TABLE_NAME)
    seed_table(TEST_TABLE_NAME, 3)
    items = list_items(TEST_TABLE_NAME)
    assert isinstance(items, list)
    assert len(items) >= 3
    assert all(isinstance(i, dict) for i in items)
    #destroy_table('test')


def test_get_item_by_id():
    """
    GIVEN articles stored in the database
    WHEN endpoint /article-list/ is called
    THEN it should return list of Article in json format that matches the schema
    """
    create_table_if_not_exists(TEST_TABLE_NAME)
    items = seed_table(TEST_TABLE_NAME, 1)
    item = items[0]
    db_item = get_item_by_id(TEST_TABLE_NAME, item['id'])
    assert isinstance(db_item, dict)
    assert item == db_item
