from tests.values import *
from moveit_fitness.seed_data import seed_table
from moveit_fitness.queries import get_sample_item
from moveit_fitness.tables import check_table_exists
from moveit_fitness.models import Event, Member, Reservation

EXPECTED_KEYS = {
    "Event": ['id', 'name', 'start_datetime', 'end_datetime', 'slots'],
    "Member": ['id', 'name', 'start_date', 'end_date'],
    "Reservation": ['id', 'event_id', 'member_id', 'waitlisted', 'timestamp']
}

def test_event_init():
    """
    GIVEN item from events table
    WHEN Event.__init__ is called
    THEN it should return a valid Event object with the expected keys
    """
    assert(check_table_exists('events'))
    seed_table('events', 1)
    event_dict = get_sample_item('events')
    event = Event(**event_dict)
    assert all(k in vars(event) for k in EXPECTED_KEYS['Event'])


def test_member_init():
    """
    GIVEN item from members table
    WHEN Member.__init__ is called
    THEN it should return a valid Member object with the expected keys
    """
    assert(check_table_exists('members'))
    seed_table('members', 1)
    member_dict = get_sample_item('members')
    member = Member(**member_dict)
    assert all(k in vars(member) for k in EXPECTED_KEYS['Member'])


def test_reservation_init():
    """
    GIVEN item from events table
    WHEN Reservation.__init__ is called
    THEN it should return a valid Reservation object with the expected keys
    """
    assert(check_table_exists('reservations'))
    seed_table('reservations', 1)
    reservation_dict = get_sample_item('reservations')
    res = Reservation(**reservation_dict)
    assert all(k in vars(res) for k in EXPECTED_KEYS['Reservation'])