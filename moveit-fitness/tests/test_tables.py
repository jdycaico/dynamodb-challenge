from moveit_fitness.tables import *
from moveit_fitness.seed_data import seed_table
from tests.values import *
from pprint import pprint as pp

def test_get_table_size():
    """
    GIVEN articles stored in the database
    WHEN endpoint /article-list/ is called
    THEN it should return list of Article in json format that matches the schema
    """
    create_table_if_not_exists(TEST_TABLE_NAME)
    seed_table(TEST_TABLE_NAME, 3)
    resp = get_table_size(TEST_TABLE_NAME)
    destroy_table('test')