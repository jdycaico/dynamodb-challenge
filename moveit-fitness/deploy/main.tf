data "aws_ami" "amazonlinux" {
  owners      = ["amazon"]
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
}

resource "aws_key_pair" "main" {
  key_name   = var.project
  public_key = file("/Users/jonathan.dycaico/_auth/aws/moveit.pub")
}

resource "aws_instance" "server" {
  ami                  = data.aws_ami.amazonlinux.id
  instance_type        = "t3.small"
  subnet_id            = aws_subnet.public.id
  security_groups      = [aws_security_group.main.id]
  iam_instance_profile = aws_iam_instance_profile.ec2-role-instance-profile.id
  user_data            = file("init.sh")
  key_name             = aws_key_pair.main.key_name
  tags = {
    Name = "${var.project}_server"
  }
}

/*
resource aws_ebs_volume "server-ebs" {
  availability_zone = var.az
  size = 30
}

resource "aws_ebs_volume_attachment" "server-ebs-attach" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.example.id
  instance_id = aws_instance.web.id
}
*/