#!/bin/bash
# these run as an unknown user, use explicit paths

sudo su
echo $USER > /home/ec2-user/init-script-sudo-user #just curious
yum -y install git
aws configure set region us-west-2
aws secretsmanager get-secret-value --secret-id moveit_private_sshkey --query 'SecretString' --output text > /home/ec2-user/.ssh/id_rsa
ssh-keyscan gitlab.com >> /home/ec2-user/.ssh/known_hosts
su ec2-user bash -c "git clone git@gitlab.com:jdycaico/moveit-fitness.git /home/ec2-user/moveit-fitness"
pip3 install virtualenv
python3 -m virtualenv /home/ec2-user/moveit-fitness/venv
source /home/ec2-user/moveit-fitness/venv/bin/activate
pip install -r /home/ec2-user/moveit-fitness/requirements.txt

# for better security, use GitLab Personal Access Token and clone via HTTPS like below:
# `git clone https://jdycaico-gitlabsvc:$TOKEN@gitlab.com/jdycaico/moveit-fitness.git`
