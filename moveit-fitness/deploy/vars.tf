variable "project" {
  type    = string
  default = "MOVEit"
}

variable "region" {
  type    = string
  default = "us-west-2"
}

variable "az" {
  type = string
  default = "us-west-2a"
}

variable "tables" {
  type = list(string)
  default = [
    "reservation",
    "course",
    "coache",
    "gym",
    "customer"
  ]
}

variable "primary_keys" {
  type = list(string)
  default = [
    "reservation_id",
    "course_id",
    "coach_id",
    "gym_id",
    "customer_id"
  ]
}

/*
variable "table_definitions" {
  type = list(map)
  default = [
    {
      table_name = "reservations"
      table_primary_key = "reservation_id"
    }
  ]
}*/