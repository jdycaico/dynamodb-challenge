output "ami" {
  value = data.aws_ami.amazonlinux.id
}

output "connect_command" {
  value = "ssh -i moveit.pem ec2-user@${aws_instance.server.public_ip}"
}

output "server_public_ip" {
  value = aws_instance.server.public_ip
}

/*
output "tf_key" {
  value = tls_private_key.tf_key
}*/