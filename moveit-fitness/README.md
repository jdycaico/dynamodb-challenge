# MOVEit Fitness

1. ~~In terraform, deploy an ECS instance and a DynamoDB cluster~~ done
2. Build a very basic fitness class registration system, with tests (remember this git commit)
3. Come up with small feature enhancements that are tricky from a coding perspective. develop solutions using boto3

FLAKE8, unit/integration testing (@pytest.mark.integration)
- run flake8, pytest-unit locally before scp
- scp and run all integration tests


# # Flake8
try: `flake8 --max-complexity 3` -> linter
plugins: pep8-naming, flake8-black, (flake8-isort||flake8-import-order), (flake8-bandit)

use pre-commit hooks as per https://testdriven.io/blog/python-code-quality/#pre-commit_hooks

https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.html


Let's consider what's important and prioritize working with that:
- DynamoDB queries/updates/deletes
- DynamoDB handling datetimes 
    - iso strings
    - epoch time

- Implement System Requirements:
    "Attendees get first come first served access to your event. 
    People who sign up once capacity is reached will be placed on a waiting list. 
    When a person with a registered spot cancels:
        the first person on the waitlist should be given a registered spot."
- Enforcing code quality with TDD/Pytest, isort/black/flake8
- Docstrings and type-hinting as per https://testdriven.io/blog/documenting-python/


Functionality I need to implement:
- A member can make a reservation
    - if count(reservations).where(event_id=$event_id) == event.total_slots:
        reservation.type = "waitlist"
    - else: reservation.type = "active"

- A member can cancel a reservation
    - if count(reservations{event_id=event_id}) < event.total_slots:
    - oldest_waitlist_entry = reservations{event_id=event_id}.sort(oldest_first)[0]
    - oldest_waitlist_entry.modify('type'='active')


Let's assume they will use OOP
