import logging
from pprint import pprint as pp

import boto3

import models
import queries
import seed_data
import tables

fmt = "%(funcName)s():%(lineno)i: %(message)s %(levelname)s"
logging.basicConfig(level=logging.INFO, format=fmt)

dynamodb = boto3.resource("dynamodb")  # , endpoint_url="http://localhost:8000"


def main():
    tables.reset_table("events")
    seed_data.seed_table("events")
    pp(queries.get_sample_item('events'))


if __name__ == "__main__":
    main()
