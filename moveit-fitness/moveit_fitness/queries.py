"""
The items module: Handles logic related to item-level operations

Easily perform queries, updates, and deletes to items in a DynamoDB table
"""

import logging

import boto3

fmt = "%(funcName)s():%(lineno)i: %(message)s %(levelname)s"
logging.basicConfig(level=logging.INFO, format=fmt)

dynamodb = boto3.resource("dynamodb")


def get_sample_item(table_name: str) -> dict:
    """
    Retrieves a sample item from the specified table

    Useful for validation and understanding data structure

    :param table_name: name of the DynamoDB table
    :return: dict represntation of the first returned table item
    """
    table = dynamodb.Table(table_name)
    result = table.scan(Limit=1)
    items = result["Items"]
    if len(result["Items"]) == 0:
        return False
    else:
        return items[0]


def list_items(table_name: str) -> list:
    """
    Lists all items contained in the named table

    :param table_name: name of the DynamoDB table
    :return: list of dictionaries representing the table items
    """
    table = dynamodb.Table(table_name)
    results = table.scan()
    if "Items" in results:
        items = results["Items"]
        return items


def get_item_by_id(table_name: str, item_id: str) -> dict:
    """
    Fetches a single item by id from the specified table

    :param table_name: name of the DynamoDB table
    :param id: id of the item within the table
    :return: a single dictionary representing the item
    """
    table = dynamodb.Table(table_name)
    db_item = table.get_item(Key={"id": item_id})
    return db_item["Item"]


def get_reservations_by_event_id(event_id: str) -> list:
    pass
