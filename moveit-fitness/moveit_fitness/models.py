class Event:
    def __init__(self, id, name, start_datetime, end_datetime, slots):
        self.id = id
        self.name = name
        self.start_datetime = start_datetime
        self.end_datetime = end_datetime
        self.slots = slots


class Member:
    def __init__(self, id, name, start_date, end_date):
        self.id = id
        self.name = name
        self.start_date = start_date
        self.end_date = end_date


class Reservation:
    def __init__(self, id, event_id, member_id, waitlisted, timestamp):
        self.id = id
        self.event_id = event_id
        self.member_id = member_id
        self.waitlisted = waitlisted
        self.timestamp = timestamp
