"""
The tables module: Handles logic related to table-level operations

Easily Create, Delete, and get the Size and pull sample records from a table
"""

import logging

import boto3

# import queries

fmt = "%(funcName)s():%(lineno)i: %(message)s %(levelname)s"
logging.basicConfig(level=logging.INFO, format=fmt)

client = boto3.client("dynamodb")
dynamodb = boto3.resource("dynamodb")
DESIRED_TABLES = ["events", "members", "reservations", "test"]


def create_table(table_name):
    params = {
        "TableName": table_name,
        "KeySchema": [
            {"AttributeName": "id", "KeyType": "HASH"},
        ],
        "AttributeDefinitions": [
            {"AttributeName": "id", "AttributeType": "S"}
        ],
        "ProvisionedThroughput": {
            "ReadCapacityUnits": 10,
            "WriteCapacityUnits": 10
        },
    }
    table = dynamodb.create_table(**params)
    logging.info(f"Creating {table_name}...")
    table.wait_until_exists()
    return table


def create_table_if_not_exists(table_name):
    names = list_existing_tables()
    if table_name not in names:
        create_table(table_name)
    else:
        return True


def destroy_table(table_name):
    table = dynamodb.Table(table_name)
    table.delete()
    logging.info(f"Deleting {table_name}...")
    table.wait_until_not_exists()


def reset_table(table_name):
    # could apply threading here
    logging.info(f"Resetting {table_name}...")
    existing_tables = list_existing_tables()

    if table_name in existing_tables:
        destroy_table(table_name)
    create_table(table_name)


def list_existing_tables():
    response = client.list_tables()
    table_names = response["TableNames"]
    return table_names


def reset_all_tables():
    all_tables = list_existing_tables()
    for table_name in all_tables:
        destroy_table(table_name)
    for table_name in DESIRED_TABLES:
        create_table(table_name)


def ensure_all_tables_exist():
    for table_name in DESIRED_TABLES:
        create_table_if_not_exists(table_name)


def get_table_size(table_name):
    logging.info(f"Getting size of {table_name}...")
    table = dynamodb.Table(table_name)
    result = table.scan()
    items = result["Items"]
    return len(items)


def get_all_table_sizes():
    output = {}
    names = list_existing_tables()
    for table_name in names:
        output[table_name] = get_table_size(table_name)
    return output


def check_table_exists(table_name):
    table = dynamodb.Table(table_name)
    try:
        table.table_id
    except dynamodb.exceptions.ResourceNotFoundException:
        return False
    return True
