import decimal

# I'd like to find a better way to handle this than copy-pasting code
# I can simply accept the decimals.
# They compute just finde with number comparison, etc


def replace_decimals(obj):
    # https://github.com/boto/boto3/issues/369#issuecomment-157205696
    if isinstance(obj, list):
        for i in range(len(obj)):
            obj[i] = replace_decimals(obj[i])
        return obj
    elif isinstance(obj, dict):
        for k in obj.keys():
            obj[k] = replace_decimals(obj[k])
        return obj
    elif isinstance(obj, decimal.Decimal):
        if obj % 1 == 0:
            return int(obj)
        else:
            return float(obj)
    else:
        return obj
