import json
import logging
import pathlib
import random
from datetime import date, datetime, timedelta

import boto3
from django.utils.crypto import get_random_string
from faker import Faker

import queries
import tables

appdir = pathlib.Path(__file__).parent


fmt = "%(funcName)s():%(lineno)i: %(message)s %(levelname)s"
logging.basicConfig(level=logging.INFO, format=fmt)

CONFIG = json.loads((appdir / "config.json").read_bytes())
dynamodb = boto3.resource("dynamodb")

fake = Faker()

DEFAULT_SEED_COUNT = 20


def generate_enrollment_dates():
    gym_opening_date = date.fromisoformat(CONFIG["date_opened"])
    start_date = fake.date_between(gym_opening_date, date.today())
    if random.choice([True, False]):
        end_date = fake.date_between(start_date, date.today())
    else:
        end_date = None
    return (start_date, end_date)


# first, arrive at the date based on window
# then, generate a start_time based on open_hours
# return start_time, start_time + timedelta(hours=1)
def generate_session_times():
    one_week_ago = date.today() - timedelta(days=7)
    two_weeks_hence = date.today() + timedelta(days=14)

    session_day = fake.date_between(one_week_ago, two_weeks_hence)

    hours = CONFIG["business_hours"][session_day.weekday()]
    hours = {key: int(hours[key] / 100) for key in hours}

    start_hour = random.randint(hours["open"], (hours["close"] - 1))
    start_time = datetime.strptime(str(start_hour), "%H").time()

    start_datetime = datetime.combine(session_day, start_time)
    end_datetime = start_datetime + timedelta(hours=1)

    return (start_datetime, end_datetime)


def generate_event():
    start_datetime, end_datetime = generate_session_times()

    return {
        "id": f"EVT-{get_random_string(7)}",
        "name": fake.bs().title(),
        "start_datetime": start_datetime.isoformat(),
        "end_datetime": end_datetime.isoformat(),
        "slots": random.randint(12, 50),
    }


def generate_member():
    start_date, end_date = generate_enrollment_dates()
    if isinstance(end_date, date):
        end_date = end_date.isoformat()

    return {
        "id": f"USR-{get_random_string(7)}",
        "name": fake.name(),
        "start_date": start_date.isoformat(),
        "end_date": end_date,
    }


def generate_reservation():
    # Events and Members must be seeded before Reservations
    events = queries.list_items("events")
    event_ids = [e["id"] for e in events]
    members = queries.list_items("members")
    member_ids = [m["id"] for m in members]

    return {
        "id": f"RSV-{get_random_string(7)}",
        "event_id": random.choice(event_ids),
        "member_id": random.choice(member_ids),
        "waitlisted": random.choice([True, False]),
        "timestamp": datetime.now().isoformat(),
    }


def generate_test_item():
    return {
        "id": f"TST-{get_random_string(7)}",
        "name": fake.name(),
        "date": fake.date_time().isoformat(),
    }


def seed_table(table_name, count=DEFAULT_SEED_COUNT):
    logging.info(f"Seeding {table_name}...")
    function_switch = {
        "events": generate_event,
        "members": generate_member,
        "reservations": generate_reservation,
        "test": generate_test_item,
    }
    generated_items = []
    for i in range(count):
        item: dict = function_switch[table_name]()
        dynamodb.Table(table_name).put_item(Item=item)
        generated_items.append(item)
    table_size = tables.get_table_size(table_name)
    logging.info(f"{table_name} now has {table_size} items.")
    return generated_items


def seed_all_tables():
    existing_tables = tables.list_existing_tables()
    logging.info(f"existing_tables:{existing_tables}")
    for table_name in existing_tables:
        seed_table(table_name, DEFAULT_SEED_COUNT)


if __name__ == "__main__":
    courses = queries.list_items("events")
    print(courses)
