# DynamoDB-Challenge

This repo is the results of preparing for, and taking, a python challenge for a job interview related to the DynamoDB AWS Service.

Folders:

## Moveit-Fitness
Contains my preparatory work to try to imagine and structure how a fitness reservation system might work in DynamoDB.

## SecDevPython
Contains my code and tests from the actual challenge, which was time-limited. Challenge instructions and interviewer-provided functions have been omitted out of necessity, as they are not my work. Certain notes have been rewritten in my own words to provide a modicum of context.

Due to the time-sensitive nature of the challenge, the `pytest` functions are not comprehensive. In addition, you will find references to undeclared functions that I have been forced to remove.

