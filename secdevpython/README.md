# SecDevPythonChallenge

## Purpose
This is a coding challenge to be given to prospective developers. It is based in python and is sourced from an actual production system (Diana's fitness registration website), thus including some real world practical requirements.

## ProdSec Setup Instructions
- Make sure you have access to a designated non-PRD AWS account

- Git clone this repo to your local
 
- Update the deployer key and name in the Terraform to your own public key and name. This will allow you to connect to the host (very important!). To update your name and key, go to terraform/var/tf -> `variable "deployer"`
  ```
  TIP: cat ~/.ssh/id_rsa.pub
  ```
  
- Update the candidate key (which should have been provided to you) and name in terraform/var/tf -> `variable "candidate"`

- Run Terraform to spin up the dynamo tables and challenge host, specifying the desired region:
  ```
  # Example
  cd terraform
  aws-vault exec dwong-test -- terraform apply -var="region=us-west-2"
  ```
  
- Command to ssh into the ec2 (go to EC2 console and find public IP for below):
  Note: terraform apply will output the command at the end as well.
  ```
  ssh ec2-user@34.218.254.16
  ```

- Provide the login to recruiting (i.e. [candidate_name]@34.218.254.16)

## ProdSec Grading Instructions
- SSH into their EC2 and find their challenge code, probably under var/challenge or you can use this command:
  - ```find . -name "challenge*"```
- Replace challenge1,2,3.py to the project folder of the branch you checked out when creating test env:
  ```
  scp -r ec2-user@35.86.146.210:"/var/challenge/challenge*" \
    /path/to/secdevpythonchallenge/src/
  ```
- The /tests folder has pytests you can use to help you grade the tests, use this command to run them:
  `python3 -m pytest -s`
- To quickly disable a test, change the name prefix from "test_" to "test"
- Go to the AWS console and take a screenshot of their DynamoDB before running the test, then run the test and take another screenshot of the after effects of the test. A side by side view should make it easier to view.
- To delete all lines in vi:
  - ```:%d```

## Interviewee Instructions

### Business Problem: 
You are creating a fitness class registration system, and your classes are popular! This is a good problem to have, but your space can only accommodate so many people, and demand outpaces availability.

### System Requirement: 
Attendees get first come first served access to your event. People who sign up once capacity is reached will be placed on a waiting list. When a person with a registered spot cancels, the first person on the waitlist should be given a registered spot.

### Time Frame: 
You will have 24 hours to complete these exercises

### Hints:
 - Connect to the challenge host as follows: ssh {username}@{publicIP}

 - The files to be edited can be found under /var/challenge

 - The challenges increase in complexity, each building upon the other. You are welcome and encouraged to reuse provided code (don't worry about duplicated code for this exercise). 

 - You'll be doing a lot of DynamoDB queries/updates/deletes with python. https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.Python.03.html

 - Challenges 1 and 3 involve list iteration and manipulation.

 - For a full dump of the classes table you can use this cli command (candidate name is in var.tf file - ${var.candidate["name"]}): 
    ```
      aws dynamodb scan --table-name Classes_<candidate_name> --region us-west-2
    ```
 - To cleanup data and restart, use the cleanup.py class, substituting the appropriate class_date value

 - You'll need to install packages with pip (requirements.txt)

 - You'll want to make use of a virtual environment to avoid path/package issues. Root is *not* required: https://docs.python.org/3/tutorial/venv.html
