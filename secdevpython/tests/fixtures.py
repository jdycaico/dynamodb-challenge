import random

import pytest
from faker import Faker

fake = Faker()


@pytest.fixture(autouse=False)
def mock_future_class():
    def _mock_future_class():
        capacity = random.choice([1, 5, 10])
        return {
            "capacity": str(capacity),
            "class_date": fake.date(),
            "class_time": fake.time("%I:%M %p"),
            "spots_taken": str(random.randint(0, capacity)),
        }

    return _mock_future_class
