from faker import Faker

from challenge.challenge2 import getClassByDate, getClassCapacity, signup
from challenge.challenge3 import listReservationsByDate
from challenge.cleanup import clearSignupsForDate

fake = Faker()
from pprint import pprint as pp

from tests.values import *


def main():
    visualize_data_challenge3()


def visualize_data_challenge3():
    signups = listReservationsByDate(C3_CLASS_DATE)
    reservations = [s for s in signups if s["reserve_position"] > 0]
    waitlist = [s for s in signups if s["waitlist_position"] > 0]

    for r in reservations:
        r.pop("signup_time")

    for w in waitlist:
        w.pop("signup_time")

    print("Reservations")
    pp(reservations)
    print("\nWaitlist")
    pp(waitlist)


def seed_test_user_challenge3():
    class_date = C3_CLASS_DATE
    capacity = getClassCapacity(getClassByDate(class_date))
    signup(username=C3_TEST_USER, class_date=class_date, capacity=capacity)


def reset_and_seed_challenge3():
    clearSignupsForDate(C3_CLASS_DATE)
    for i in range(len(C3_USERNAMES)):
        username = C3_USERNAMES[i]
        class_date = C3_CLASS_DATE
        capacity = getClassCapacity(getClassByDate(class_date))
        signup(username, class_date, capacity)


def seed_challenge2():
    for i in range(3):
        username = fake.email()
        class_date = "2023-10-15"
        capacity = getClassCapacity(getClassByDate(class_date))
        signup(username, class_date, capacity)

    for i in range(8):
        username = fake.email()
        class_date = "2023-10-17"
        capacity = getClassCapacity(getClassByDate(class_date))
        signup(username, class_date, capacity)


if __name__ == "__main__":
    main()
