import json
import random
import warnings
from datetime import date
from pprint import pprint as pp

from faker import Faker

from challenge.challenge2 import getClassByDate, getClassCapacity, signup
from challenge.challenge3 import (
    cancelReservation,
    decrementPositionsAbove,
    deleteReservation,
    getReservation,
    listReservationsByDate,
    promoteTopWaitlistEntry,
)
from challenge.db import dynamodb
from tests.data import reset_and_seed_challenge3, seed_test_user_challenge3
from tests.fixtures import mock_future_class
from tests.values import C3_CLASS_DATE, C3_TEST_USER, C3_USERNAMES, EXPECTED_SIGNUP_KEYS

fake = Faker()


def test_promote_top_waitlist_entry():
    """
    GIVEN a known dataset in the table
    WHEN an promoteTopWaitlistEntry is called
    THEN the reservation with waitlist_position 1 should end up with reserve_position 5
    """
    reset_and_seed_challenge3()
    reservations = listReservationsByDate(C3_CLASS_DATE)

    res = getReservation(username="orson.card", class_date=C3_CLASS_DATE)
    assert res["waitlist_position"] == 1

    promoteTopWaitlistEntry(C3_CLASS_DATE, capacity=5)

    test_res = getReservation(username="orson.card", class_date=C3_CLASS_DATE)
    assert test_res["reserve_position"] == 5


def test_decrement_positions_above():
    """
    GIVEN a known dataset in the table
    WHEN an item is deleted and decrementPositionsAbove is called
    THEN all relevant positions greater than the position should be decremented
    """
    reset_and_seed_challenge3()
    reservations = listReservationsByDate(C3_CLASS_DATE)

    res = getReservation(username="clive.lewis", class_date=C3_CLASS_DATE)

    delete_result = deleteReservation(username="clive.lewis", class_date=C3_CLASS_DATE)
    pp(f"delete_result={delete_result}")
    decrementPositionsAbove(
        position=res["waitlist_position"], class_date=C3_CLASS_DATE, waitlist=False
    )

    test_res = getReservation(username="frank.herbert", class_date=C3_CLASS_DATE)

    assert test_res["reserve_position"] == 2


def test_get_reservation():
    """
    GIVEN a username and class_date
    WHEN getReservation is invoked
    THEN it should return a single dictionary with the expected keys
    """
    seed_test_user_challenge3()
    signup = getReservation(username=C3_TEST_USER, class_date=C3_CLASS_DATE)
    assert isinstance(signup, dict)
    assert sorted(signup.keys()) == EXPECTED_SIGNUP_KEYS


def test_delete_reservation():
    """
    GIVEN an existing username and class_date
    WHEN deleteReservation is invoked
    THEN it should return a boolean failure
    """
    seed_test_user_challenge3()
    resp = deleteReservation(username=C3_TEST_USER, class_date=C3_CLASS_DATE)
    assert resp == True


def test_delete_reservation_noex_user():
    """
    GIVEN a nonexistent username and class_date
    WHEN deleteReservation is invoked
    THEN it should return a boolean failure
    """
    resp = deleteReservation(username="nonexistent.user", class_date=C3_CLASS_DATE)
    assert resp == False


def test_list_reservations_by_date():
    # GIVEN a class_date
    # WHEN listReservationsByDate is called against it
    # THEN the list of LiveClassSignup items matching that date is returned

    reset_and_seed_challenge3()
    signups = listReservationsByDate(C3_CLASS_DATE)
    assert isinstance(signups, list)
    assert all(isinstance(s, dict) for s in signups)
    assert all(sorted(s.keys()) == EXPECTED_SIGNUP_KEYS for s in signups)
    assert len(signups) == len(C3_USERNAMES)


def test_cancel_reservation():
    seed_test_user_challenge3()
    class_date = C3_CLASS_DATE
    resp = cancelReservation(
        class_date=class_date,
        username="seven.seas",
        capacity=getClassCapacity(getClassByDate(class_date)),
    )
    pp(resp)
