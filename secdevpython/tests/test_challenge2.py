import json
import random
import warnings
from datetime import date
from pprint import pprint as pp

from faker import Faker

from challenge.challenge1 import getClasses
from challenge.challenge2 import calculateClassPositions, getClassByDate, getSpotNumber
from challenge.db import dynamodb
from tests.fixtures import mock_future_class
from tests.values import CLASS_DATES

fake = Faker()


def test_get_class_by_date():
    """
    GIVEN a class_date string
    WHEN getClassByDate is called
    THEN a class with the same class_date exists in the database
    """
    class_date = random.choice(CLASS_DATES)
    my_class = getClassByDate(class_date)
    assert isinstance(my_class, dict)

    classes_json = getClasses()
    all_classes = json.loads(classes_json)
    assert isinstance(all_classes, list)
    test_class = next(c for c in all_classes if c["class_date"] == class_date)
    assert isinstance(test_class, dict)

    assert my_class["class_date"] == test_class["class_date"]


def test_calculate_class_positions_sn_lt_cap():
    """
    GIVEN a spotNumber and capacity where spotNumber is less than capacity
    WHEN calculateClassPositions is called
    THEN an appropriate reserve_position,waitlist_position tuple is generated
    """
    spotNumber = 3
    capacity = 10
    reserve_position, waitlist_position = calculateClassPositions(
        spotNumber=spotNumber, capacity=capacity
    )
    assert reserve_position == spotNumber
    assert waitlist_position == 0


def test_calculate_class_positions_sn_eq_cap():
    """
    GIVEN a spotNumber and capacity where spotNumber is equal to capacity
    WHEN calculateClassPositions is called
    THEN an appropriate reserve_position,waitlist_position tuple is generated
    """
    spotNumber = 5
    capacity = 5
    reserve_position, waitlist_position = calculateClassPositions(
        spotNumber=spotNumber, capacity=capacity
    )
    assert reserve_position == spotNumber
    assert waitlist_position == 0


def test_calculate_class_positions_sn_gt_cap():
    """
    GIVEN a spotNumber and capacity where spotNumber is greater to capacity
    WHEN calculateClassPositions is called
    THEN an appropriate reserve_position,waitlist_position tuple is generated
    """
    spotNumber = 11
    capacity = 10
    reserve_position, waitlist_position = calculateClassPositions(
        spotNumber=spotNumber, capacity=capacity
    )
    assert reserve_position == 0
    assert waitlist_position == spotNumber - capacity


def test_challenge_2a():
    """
    GIVEN the specifications for Challenge 2a
    WHEN the tests are run
    THEN the entries in LiveClassSignup should match the specifications
    """
    spotNumber_1015 = getSpotNumber("2023-10-15")
    spotNumber_1017 = getSpotNumber("2023-10-17")

    assert (spotNumber_1015 - 1) == 3
    assert (spotNumber_1017 - 1) == 8
