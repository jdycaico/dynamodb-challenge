from datetime import date
from pprint import pprint as pp

from faker import Faker

from challenge.challenge1 import sortClasses, validISODate
from tests.fixtures import mock_future_class

fake = Faker()


def test_sort_classes(mock_future_class):
    """
    GIVEN a list of dictionaries containing a class_date key
    WHEN sortClasses is called
    THEN the same list should be returned, sorted by class_date ASC
    """
    classes = []
    for i in range(5):
        classes.append(mock_future_class())
    pp(classes)
    pp(type(classes[0]["class_date"]))
    sorted_classes = sortClasses(classes)

    assert isinstance(sorted_classes, list)
    assert len(sorted_classes) > 0

    class_dates = [c["class_date"] for c in sorted_classes]
    sorted_class_dates = sorted(class_dates, key=lambda c: date.fromisoformat(c))

    assert class_dates == sorted_class_dates


def test_valid_iso_date():
    """
    GIVEN a valid ISO date string
    WHEN validISODate is invoked on the date string
    THEN the date string should be judged as valid
    """
    assert validISODate(fake.date())


def test_valid_iso_date_random_string():
    """
    GIVEN a random string
    WHEN validISODate is called
    THEN the random string should be judged as invalid
    """
    assert not validISODate(fake.pystr())
