CLASS_DATES = ["2023-10-15", "2023-10-16", "2023-10-17", "2023-10-18", "2023-10-19"]

C3_CLASS_DATE = "2023-10-16"

C3_USERNAMES = [
    "john.tolkein",
    "clive.lewis",
    "frank.herbert",
    "william.gibson",
    "joanne.rowling",
    "orson.card",
    "douglas.adams",
    "robert.jordan",
]

C3_TEST_USER = "kim.dotcom"

EXPECTED_SIGNUP_KEYS = sorted(
    ["class_date", "reserve_position", "username", "waitlist_position", "signup_time"]
)
