import boto3

dynamodb = boto3.resource(
    "dynamodb",
    region_name="us-west-2",
    endpoint_url="https://dynamodb.us-west-2.amazonaws.com",
)

dyn_client = boto3.client("dynamodb", region_name="us-west-2")
