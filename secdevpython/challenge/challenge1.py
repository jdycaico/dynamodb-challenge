import datetime
import json
import logging
import time

import pytz

fmt = "%(funcName)s():%(lineno)i: %(message)s %(levelname)s"
logging.basicConfig(level=logging.INFO, format=fmt)

from datetime import date, datetime, timedelta

from boto3.dynamodb.conditions import Attr, Key
from botocore.exceptions import ClientError
from dateutil import parser
from db import dynamodb
from pytz import timezone


def main():
    #getClasses()
    pass

# Challenge 1: getClasses() retrieves fitness classes that are in the future. The goal is to sort the classes by class_date


def validISODate(date_string: str) -> bool:
    """
    Validates that a date string is in ISO-8601 format

    :param date: a string date representation
    :return: boolean verdict of compliance to format
    """
    assert isinstance(date_string, str)
    logging.info({"date_string": date_string, "type": type(date_string)})
    try:
        date.fromisoformat(date_string)
    except ValueError:
        logging.error(f"date not in ISO format: date_string={date_string}")
        return False
    return True


def sortClasses(classes: list) -> list:
    """
    Sorts a list of dictionaries by class_date, in ascending order
    Casts string dates to datetime.date objects for reliable sorting

    :param classes: list of 'class' dictionaries
    :return: a sorted list of 'class' dictionaries
    """
    assert isinstance(classes, list)
    if len(classes) > 0:
        class_dates = [c["class_date"] for c in classes]
        assert all(validISODate for date_str in class_dates)
        return sorted(classes, key=lambda c: date.fromisoformat(c["class_date"]))
    else:
        logging.info("sortClasses called with empty list")
        return []


if __name__ == "__main__":
    main()
