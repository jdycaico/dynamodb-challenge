import datetime
import logging
from pprint import pprint as pp

import boto3
from boto3.dynamodb.conditions import Attr, Key
from botocore.exceptions import ClientError

fmt = "%(funcName)s():%(lineno)i: %(message)s %(levelname)s"
logging.basicConfig(level=logging.INFO, format=fmt)

from challenge1 import validISODate
from challenge2 import getClassByDate, getClassCapacity

from challenge.db import dyn_client, dynamodb


# Challenge 3a: Move the waitlisted student up one position and remove their reservation on the reserved list, for each class
def main():
    class_date = "2023-10-16"
    username = "kim.dotcom"
    capacity = getClassCapacity(getClassByDate(class_date))

    cancelReservation(class_date, capacity, username)


def listReservationsByDate(class_date):
    """
    For a given date, returns all matching LiveClassSignup items
    Both waitlist entries and reservations
    As a sorted list of dictionaries

    :param class_date: an ISO-8601 string date
    :return: a list of Signup items matching that date
    """
    assert validISODate(class_date)
    table = dynamodb.Table("LiveClassSignup_jonathan")

    response = table.query(
        KeyConditionExpression=Key("class_date").eq(class_date),
    )
    assert "Count" in response
    if response["Count"] > 0:
        sorted_items = sorted(response["Items"], key=lambda i: i["reserve_position"])
        return sorted_items
    else:
        logging.info(f"No signups found for {class_date}...")
        return []


def getReservation(username: str, class_date: str) -> dict:
    """
    Searches for a single item in LiveClassSignup by the received arguments
    This should yield up to one item given the logic of challenge2.signup

    :param username: a lowercase string username
    :param class_date: an ISO-8601 string date
    :return: a list of Signup items matching that date
    """
    table = dynamodb.Table("LiveClassSignup_jonathan")
    query_response = table.query(
        KeyConditionExpression=Key("username").eq(username)
        & Key("class_date").eq(class_date)
    )
    assert "Count" in query_response
    if query_response["Count"] == 1:
        return query_response["Items"][0]
    elif query_response["Count"] == 0:
        logging.error(
            f"""Could not find item with 
                        user:{username}; date:{class_date}"""
        )
        return None
    elif query_response["Count"] > 1:
        logging.error(
            f"""Multiple items found matching query -- 
                    user:{username}; date:{class_date}"""
        )


def deleteReservation(username: str, class_date: str) -> dict:
    """
    Searches for and deletes a single item in LiveClassSignup
    Corresponding to the received arguments

    :param class_date: an ISO-8601 string date
    :param username: a lowercase string username
    :return: a boolean indicating whether or not the delete was successful
    """
    table = dynamodb.Table("LiveClassSignup_jonathan")
    try:
        table.delete_item(
            Key={"username": username, "class_date": class_date},
            ConditionExpression="attribute_exists(username) AND attribute_exists(class_date)",
        )
    except dyn_client.exceptions.ConditionalCheckFailedException:
        logging.error(
            f"""Failed to delete item with 
                    username:{username}; class_date:{class_date}"""
        )
        return False
    return True


# 3d.
def updateReservation(reservation: dict) -> bool:
    """
    Performs modifications to the position fields for items in the LiveClassSignup table
    To match the values provided in the reservation dict

    :param reservation: a dict containing updated values for the item
    :return: a boolean indicating success or failure
    """
    table = dynamodb.Table("LiveClassSignup_jonathan")
    res = reservation
    try:
        table.update_item(
            Key={"class_date": res["class_date"], "username": res["username"]},
            UpdateExpression="set reserve_position=:r, waitlist_position=:w",
            ExpressionAttributeValues={
                ":r": res["reserve_position"],
                ":w": res["waitlist_position"],
            },
            ReturnValues="UPDATED_NEW",
            ConditionExpression="attribute_exists(username) AND attribute_exists(class_date)",
        )
    except dyn_client.exceptions.ConditionalCheckFailedException:
        logging.error(
            f"""Failed to update item with 
                    username:{res['username']}; class_date:{res['class_date']}"""
        )
        return False
    return True


def promoteTopWaitlistEntry(class_date, capacity):
    """
    Takes the waitlist entry with the waitlist_position of 1
    Sets its reserve_position to the maximum available
    And its waitlist_position to 0

    :param class_date: an ISO-8601 string date
    :param capacity: the capacity of the class in question
    :return: None
    """
    res = listReservationsByDate(class_date)
    top_waitlist_res = next(r for r in res if r["waitlist_position"] == 1)
    assert isinstance(top_waitlist_res, dict)
    top_waitlist_res["waitlist_position"] = 0
    top_waitlist_res["reserve_position"] = capacity

    updateReservation(top_waitlist_res)


def decrementPositionsAbove(position: int, class_date: str, waitlist: bool) -> None:
    """
    Decrements the position number for all reservations or waitlist items for the given date
    whose positions are higher in number than than the `position` parameter

    :param position: integer denoting position in reservation/waitlist hierarchy
    :param class_date: an ISO-8601 string date
    :param waitlist: switch specifying if we are operating on waitlist items or not
    :return: None
    """
    reservations = listReservationsByDate(class_date)
    target_field = "waitlist_position" if waitlist else "reserve_position"
    pp(f"reservations[0]={reservations[0]}")
    reservations_to_decrement = [r for r in reservations if r[target_field] > position]
    for t in reservations_to_decrement:
        t[target_field] -= 1
        updateReservation(t)


if __name__ == "__main__":
    main()