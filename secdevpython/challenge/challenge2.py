import base64
import datetime
import json
import logging
from datetime import datetime

import boto3
from boto3.dynamodb.conditions import Attr, Key
from botocore.exceptions import ClientError
from challenge1 import getLocationDetails
from faker import Faker

fmt = "%(funcName)s():%(lineno)i: %(message)s %(levelname)s"
logging.basicConfig(level=logging.INFO, format=fmt)

from db import dynamodb

fake = Faker()
# Challenge 2a: Retrieve the signup function's inputs using challenge1 code. Achieve the following outcome: 3 students endolled for the 2023-10-15 class, and 8 students for the 2023-10-17 class.


def main():
    # for Challenge3
    for i in range(6):
        username = f"{fake.first_name().lower()}.{fake.last_name().lower()}@mega.nz"
        class_date = "2023-10-16"
        capacity = getClassCapacity(getClassByDate(class_date))
        signup(username, class_date, capacity)

    """
    for i in range(3):
        username = fake.email()
        class_date = "2023-10-15"
        capacity = getClassCapacity(getClassByDate(class_date))
        signup(username, class_date, capacity)

    for i in range(8):
        username = fake.email()
        class_date = "2023-10-17"
        capacity = getClassCapacity(getClassByDate(class_date))
        signup(username, class_date, capacity)
    """


def getClassByDate(class_date: str) -> dict:
    """
    Assuming each date has only one class associated (other functions alogging.infoear to)
    Returns the only class dict matching the class_date from the Class table

    :param class_date: date string reprenting the date of the class
    :return: a dictionary representing the class that takes place on that date
    """
    table = dynamodb.Table("Classes_jonathan")
    logging.info(f"class_date={class_date}")
    scan_response = table.scan(
        FilterExpression=Attr("class_date").eq(class_date),
    )
    logging.info(scan_response)
    assert "Items" in scan_response
    assert len(scan_response["Items"]) == 1
    return scan_response["Items"][0]


def getClassCapacity(my_class: dict) -> int:
    """
    Given a table item from Classes, looks up the class capacity from Locations

    :param my_class: dict representation of an item from the Classes table
    :return: the integer representation of the capacity for that class
    """
    assert isinstance(my_class, dict)
    assert "location" in my_class
    location_entry = getLocationDetails(my_class["location"])
    capacity_string = location_entry["capacity"]
    return int(capacity_string)


def calculateClassPositions(spotNumber: int, capacity: int) -> tuple:
    """
    Calculates the reserve_position and waitlist_position
    According to the values of the input variables spotNumber and capacity

    :param spotNumber: equal to count(currently_enrolled_students) + 1
    :param capacity: number of students that the class can hold
    :return: a tuple containing (reserve_position, waitlist_position)
    """
    assert isinstance(spotNumber, int)
    assert isinstance(capacity, int)
    logging.info(f"spotNumber={spotNumber}, capacity={capacity}")
    reserve_pos = waitlist_pos = 0
    utilization = spotNumber - 1
    if utilization >= capacity:
        full_class = True
        waitlist_pos = spotNumber - capacity
    else:
        full_class = False
        reserve_pos = spotNumber
    logging.info(f"full class={full_class}")
    logging.info(f"reserve={reserve_pos}, waitlist={waitlist_pos}")
    return (reserve_pos, waitlist_pos)


# Challenge 2b: For the signup, determine the positions on the reserve list and the waitlist.

# Removed for legal reasons


if __name__ == "__main__":
    main()
