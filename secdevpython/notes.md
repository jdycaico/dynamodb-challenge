check tests folder, this is part of grading: python3 -m pytest -s

Datetimes in dynamodb:
- filter expression requires comparison
- looks like it's date=x, y=x
- prefer the efficient way, which is a filter expression


Write my own tests (TDD)
- happy path first, then permutations
- make sure it can catch edge cases, weird data
- https://docs.pytest.org/en/6.2.x/fixture.html#factories-as-fixtures


TODO: 
- write test then function for challenge1.removeTodaysPastClasses
- write test then function for challenge2.validateSpotAvailable
- write a test for challenge2.getClassCapacity
- write a test for challenge3.cancelReservation
- At end, clean up imports, logging and such (unified logger?)
- At the end, find/replace all single-quotes with doubles



keep in mind that getClasses actually modifies some fields on the Classes table items


# # Challenge3:
At present, cancelReservation does remove the item from LiveClassSignup
    - ensure Item Not Found is caught
Considering breaking the function out into 3 parts


In the future, we'll want to allow a cancellation from the waitlist as well
    This will only result in advanceWaitlistPositions()


We're gonna delete Clive.Lewis (2)

Expected Behavior:
- frank.herbert: 3 -> 2
- william.gibson: 4 -> 3
- joanne.rowling: 5 -> 4


Wondering if I can't simply modify one key on the signup and dictionary-unpack it into the reorder function
(extra key: signup_time)

Considering renaming "signup" in challenge3 to be "reservation"
    subtypes: waitlisted, active

    reshufflePositions or whatever just takes a dict and applies it